# The Droid You're Looking For

Robots.txt page but only someone with Google's user agent string can visit the page

* Desc: Sometimes you need a little Star Wars in your life to come across the flag
* Flag: `TUCTF{463nt_6006l3_r3p0rt1n6_4_r0b0t}`
* Hint: Only our Google overlords can see the page made for them

## How To:
1. Realizing it's a `robots.txt` chal, go to `robots.txt`
2. Redirected to `nope.html`...it mentions agency...something with the user agent?
3. Make sure the user agent has `Googlebot` in it and it should give you the _real_ `robots.txt` page
4. Visit `googleagentflagfoundhere.html` to get the flag
