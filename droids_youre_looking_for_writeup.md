# The Droids You're Looking For -- TUCTF 2019

This challenge is a mix of a `robots.txt` challenge and a user agent challenge, and there is no better way to combine them then to think of web crawlers. For this challenge, once you recognize there is a `robots.txt` page, you notice that there is something preventing you from getting there. Some clues about agency hint that maybe the user agent is what you should go towards, and the hints about Google lead to the `Googlebot` user agent. Visiting `robots.txt` directs you to `googleagentflagfoundhere.html` where the flag is located.
